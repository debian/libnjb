/**
 * \file usb_io.c
 *
 * This file contain some USB-specific code that is used by all
 * devices.
 */

#include <string.h>
#include <stdint.h>
#include <usb.h>
#include "base.h"
#include "libnjb.h"
#include "ioutil.h"
#include "usb_io.h"
#include "njb_error.h"

extern int __sub_depth;

/* USB control message data phase direction */
#ifndef USB_DP_HTD
#define USB_DP_HTD              (0x00 << 7)     /* host to device */
#endif
#ifndef USB_DP_DTH
#define USB_DP_DTH              (0x01 << 7)     /* device to host */
#endif

/* USB Feature selector HALT */
#ifndef USB_FEATURE_HALT
#define USB_FEATURE_HALT        0x00
#endif

static int usb_get_endpoint_status(usb_dev_handle *dev,
				   int ep, uint16_t* status)
{
  return (usb_control_msg(dev,
                          USB_DP_DTH|USB_RECIP_ENDPOINT,
                          USB_REQ_GET_STATUS,
                          USB_FEATURE_HALT,
                          ep,
                          (char *) status,
                          2,
                          USBTIMEOUT));
}

static int usb_clear_stall_feature(usb_dev_handle *dev, int ep)
{
  return (usb_control_msg(dev,
                          USB_RECIP_ENDPOINT,
                          USB_REQ_CLEAR_FEATURE,
                          USB_FEATURE_HALT,
                          ep,
                          NULL,
                          0,
                          USBTIMEOUT));
}


/**
 * This function writes a number of bytes from a buffer
 * to a devices OUT endpoint.
 *
 * @param njb the jukebox object to use
 * @param buf the buffer to send bytes from
 * @param nbytes the number of bytes to write
 * @return the number of bytes actually written
 */
ssize_t usb_pipe_write(njb_t *njb, void *buf, size_t nbytes)
{
  ssize_t bwritten = -1;

  /*
   * Used for all libnjb enabled platforms.
   * This section might be the source of timeout problems so
   * it is currently being tested. Also see pipe_read below.
   */

  int usb_timeout = 10 * nbytes; /* that's 10ms / byte */
  int retransmit = 10; /* Try ten times! (That should do it...) */

  /* Set a timeout floor */
  if (usb_timeout < USBTIMEOUT)
    usb_timeout = USBTIMEOUT;

  while (retransmit > 0) {
    bwritten = usb_bulk_write(njb->dev, njb->usb_bulk_out_ep, buf, nbytes,
			     usb_timeout);
    if ( bwritten < 0 )
      retransmit--;
    else
      break;
  }
  if (retransmit == 0) {
    njb_error_add_string (njb, "usb_bulk_write", usb_strerror());
    return -1;
  }

  if (njb_debug(DD_USBBLK|DD_USBBLKLIM) ) {
    size_t bytes = ( njb_debug(DD_USBBLK) ) ? nbytes : 16;

    fprintf(stderr, "Bulk >>\n");
    data_dump_ascii(stderr, buf, bytes, 0);
    fprintf(stderr, "\n");
  }

  return bwritten;
}


/**
 * This function reads a chunk of bytes to a buffer from
 * a device's IN endpoint.
 *
 * @param njb the jukebox object to use
 * @param buf the buffer to store the bytes in
 * @param nbytes the number of bytes to read in
 * @return the number of bytes actually read
 */
ssize_t usb_pipe_read (njb_t *njb, void *buf, size_t nbytes)
{
  ssize_t bread = 0;

  /*
   * Used for all libnjb enabled platforms.
   * This section might be the source of timeout problems so
   * it is currently being tested. Also see pipe_write above.
   */
  int usb_timeout = 10 * nbytes; /* that's 10ms / byte */

  /* Set a timeout floor */
  if (usb_timeout < USBTIMEOUT)
    usb_timeout = USBTIMEOUT;

  if (njb_debug(DD_USBBLK|DD_USBBLKLIM))
    printf("LIBNJB: bulk read, timeout = %d\n", usb_timeout);

  bread = usb_bulk_read(njb->dev, njb->usb_bulk_in_ep, buf, nbytes,
			usb_timeout);

  /*
   * This should be changed to (bread < nbytes) asap, but needs
   * an NJB3 to test it, it cancels out short reads if I set
   * it to that, so these must first be avoided in all NJB3
   * code by properly checking length etc.
   */
  if (bread < 0) {
    int ret;
    uint16_t status;

    printf("LIBNJB: ERROR %d in usb_bulk_read\n", bread);
    printf("LIBNJB: libusb error: %s\n",
	   usb_strerror());

    ret = usb_get_endpoint_status(njb->dev, njb->usb_bulk_in_ep, &status);
    if (ret < 0) {
      printf("LIBNJB: unable to get endpoint status for bulk IN\n");
      return -1;
    }
    printf("LIBNJB: status on bulk IN (0x%02x): %04x\n",
	   njb->usb_bulk_in_ep, status);

    ret = usb_clear_stall_feature(njb->dev, njb->usb_bulk_in_ep);
    if (ret < 0)
      printf("LIBNJB: error in usb_clear_stall_feature()\n");
    else
      printf("LIBNJB: usb_clear_stall_feature() OK!\n");

    ret = usb_clear_halt(njb->dev, njb->usb_bulk_in_ep);
    if (ret < 0)
      printf("LIBNJB: error in usb_clear_halt()\n");
    else
      printf("LIBNJB: usb_clear_halt() OK!\n");
  }

  if (njb_debug(DD_USBBLK|DD_USBBLKLIM)) {
    size_t bytes = ( njb_debug(DD_USBBLK) ) ? bread : 16;

    fprintf(stderr, "Bulk <<\n");
    data_dump_ascii(stderr, buf, bytes, 0);
    fprintf(stderr, "\n");
  }

  return bread;
}


/**
 * This function sends a USB SETUP 8-byte command
 * across to endpoint 0 on the device.
 */
int usb_setup (njb_t *njb, int type, int request, int value,
	int index, int length, void *data)
{
  u_int8_t setup[8];
  usb_dev_handle *dev = njb->dev;

  if ( njb_debug(DD_USBCTL) ) {
    memset(setup, 0, 8);
    setup[0]= type;
    setup[1]= request;
    if ( value ) {
      setup[2] = value & 255;
      setup[3] = (value >> 8) & 255;
    }
    if ( index ) {
      setup[4] = index & 255;
      setup[5] = (index >> 8) & 255;
    }
    if ( length ) {
      setup[6] = length & 255;
      setup[7] = (length >> 8) & 255;
    }
  }

  if ( njb_debug(DD_USBCTL) ) {
    fprintf(stderr, "%*sSetup: ", 3*__sub_depth, " ");
    data_dump(stderr, setup, 8);
  }

  if ( usb_control_msg(dev, type, request, value, index, data, length,
		       USBTIMEOUT) < 0 ) {
    njb_error_add_string (njb, "usb_control_msg", usb_strerror());
    return -1;
  }

  if ( njb_debug(DD_USBCTL) ) {
    if ( length ) {
      fprintf(stderr, "%s", ( (type & UT_READ) == UT_READ ) ?
	      "<<" : ">>");
      data_dump_ascii(stderr, data, length, 0);
      fprintf(stderr, "\n");
    }
  }

  return 0;
}

